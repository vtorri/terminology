# Serbian translation for enlightenment
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the enlightenment package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
# Саша <Петровић>, 2014.
# Саша Петровић <salepetronije@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: enlightenment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-25 21:11+0200\n"
"PO-Revision-Date: 2020-03-08 19:40+0100\n"
"Last-Translator: Саша Петровић <salepetronije@gmail.com>\n"
"Language-Team: српски <xfce4@xfce4.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2017-08-03 05:19+0000\n"
"X-Generator: Poedit 2.2.1\n"

#: src/bin/about.c:129 src/bin/about.c:204 src/bin/termio.c:1296
#: src/bin/termio.c:1305
#, fuzzy, c-format
msgid "Copy '%s'"
msgstr "Умножи"

#: src/bin/about.c:134 src/bin/about.c:209 src/bin/termio.c:1367
msgid "Open"
msgstr "Отвори"

#: src/bin/about.c:273
msgid "Twitter: @_Terminology_"
msgstr ""

#: src/bin/about.c:275
msgid "YouTube channel"
msgstr ""

#: src/bin/about.c:289
#, c-format
msgid ""
"<b>Terminology %s</b><br>Why should terminals be boring?<br><br>This "
"terminal was written for Enlightenment, to use EFL and otherwise push the "
"boundaries of what a modern terminal emulator should be. We hope you enjoy "
"it.<br><br>Copyright © 2012-%d by:<br><br>%s<br><br>Distributed under the 2-"
"clause BSD license detailed below:<br><br>%s"
msgstr ""
"<b>Терминологија%s</b><br>Зашто да термилали буду досадни?<br><br>Овај "
"терминал је написан за Просвећење, да користи ЕФЛ и да на свој начин пробије "
"ограничења смисла о томе како савремени терминал треба да буде. Надамо се да "
"ћете уживати.<br><br>Права умножавања©2012-%d од:<br><br>%s<br>Расподељује "
"се под 2-издањем БСД лиценце са поједниностима описаним испод:<br><br>%s"

#: src/bin/colors.c:17
msgid "Terminology's developers"
msgstr ""

#: src/bin/controls.c:371
msgid "Controls"
msgstr "Управљачи"

#: src/bin/controls.c:387
msgid "New"
msgstr "Нови"

#: src/bin/controls.c:395
msgid "Split V"
msgstr "Подели У"

#: src/bin/controls.c:399
msgid "Split H"
msgstr "Подели В"

#: src/bin/controls.c:406
msgid "Miniview"
msgstr "Мали преглед"

#: src/bin/controls.c:413 src/bin/win.c:6206
msgid "Set title"
msgstr "Постави наслов"

#: src/bin/controls.c:424 src/bin/termio.c:1381 src/bin/termio.c:2787
msgid "Copy"
msgstr "Умножи"

#: src/bin/controls.c:430
msgid "Paste"
msgstr "Налепи"

#: src/bin/controls.c:436
msgid "Settings"
msgstr "Поставке"

#: src/bin/controls.c:442
msgid "About"
msgstr "О програму"

#: src/bin/controls.c:451
msgid "Grouped input"
msgstr ""

#: src/bin/controls.c:463
msgid "Close Terminal"
msgstr "Затвори терминал"

#: src/bin/gravatar.c:121 src/bin/main.c:898 src/bin/miniview.c:41
#, fuzzy, c-format
msgid "Could not create logging domain '%s'"
msgstr "Нисам успео да направим област за пријаву „%s“"

#: src/bin/keyin.c:658
msgid "Scrolling"
msgstr "Клизање"

#: src/bin/keyin.c:659
msgid "Scroll one page up"
msgstr "Клизај једну страну навише"

#: src/bin/keyin.c:660
msgid "Scroll one page down"
msgstr "Клизај једну страну наниже"

#: src/bin/keyin.c:661
msgid "Scroll one line up"
msgstr "Клизај за једну линију навише"

#: src/bin/keyin.c:662
msgid "Scroll one line down"
msgstr "Клизај за једну линију наниже"

#: src/bin/keyin.c:663
msgid "Go to the top of the backlog"
msgstr "Иди на врх исписа"

#: src/bin/keyin.c:664
msgid "Reset scroll"
msgstr "Враћати на подразумевано премицање"

#: src/bin/keyin.c:666
msgid "Copy/Paste"
msgstr "Умножи путању у главну међуоставу"

#: src/bin/keyin.c:667
msgid "Copy selection to Primary buffer"
msgstr "Умножи изабрано као текући исечак"

#: src/bin/keyin.c:668
msgid "Copy selection to Clipboard buffer"
msgstr "Умножи избор у оставу исечака"

#: src/bin/keyin.c:669
msgid "Paste Primary buffer (highlight)"
msgstr "Налепи главни исечак"

#: src/bin/keyin.c:670
msgid "Paste Clipboard buffer (ctrl+c/v)"
msgstr "Налепи исечак из оставе (ctrl+c/v)"

#: src/bin/keyin.c:672
msgid "Splits/Tabs"
msgstr "Поделе/листови"

#: src/bin/keyin.c:673
msgid "Focus the previous terminal"
msgstr "Претходни терминал у жижу"

#: src/bin/keyin.c:674
msgid "Focus the next terminal"
msgstr "Наредни терминал у жижу"

#: src/bin/keyin.c:675
msgid "Focus the terminal above"
msgstr "Терминал изнад у жижу"

#: src/bin/keyin.c:676
msgid "Focus the terminal below"
msgstr "Терминал испод у жижу"

#: src/bin/keyin.c:677
msgid "Focus the terminal on the left"
msgstr "Леви терминал у жижу"

#: src/bin/keyin.c:678
msgid "Focus the terminal on the right"
msgstr "Десни терминал у жижу"

#: src/bin/keyin.c:679
msgid "Split horizontally (new below)"
msgstr "Подели водоравно (нови испод)"

#: src/bin/keyin.c:680
msgid "Split vertically (new on right)"
msgstr "Подели усправно (нови  с’ десна"

#: src/bin/keyin.c:681
msgid "Create a new \"tab\""
msgstr "Направи нови „лист“"

#: src/bin/keyin.c:682
msgid "Close the focused terminal"
msgstr "Затвори терминал у жижи"

#: src/bin/keyin.c:683
msgid "Bring up \"tab\" switcher"
msgstr "Позови измењивача „језичака“"

#: src/bin/keyin.c:684
msgid "Switch to terminal tab 1"
msgstr "Пређи на Пређи на лист терминалалист терминала 1"

#: src/bin/keyin.c:685
msgid "Switch to terminal tab 2"
msgstr "Пређи на лист терминала 2"

#: src/bin/keyin.c:686
msgid "Switch to terminal tab 3"
msgstr "Пређи на лист терминала 3"

#: src/bin/keyin.c:687
msgid "Switch to terminal tab 4"
msgstr "Пређи на лист терминала 4"

#: src/bin/keyin.c:688
msgid "Switch to terminal tab 5"
msgstr "Пређи на лист терминала 5"

#: src/bin/keyin.c:689
msgid "Switch to terminal tab 6"
msgstr "Пређи на лист терминала 6"

#: src/bin/keyin.c:690
msgid "Switch to terminal tab 7"
msgstr "Пређи на лист терминала 7"

#: src/bin/keyin.c:691
msgid "Switch to terminal tab 8"
msgstr "Пређи на лист терминала 8"

#: src/bin/keyin.c:692
msgid "Switch to terminal tab 9"
msgstr "Пређи на лист терминала 9"

#: src/bin/keyin.c:693
msgid "Switch to terminal tab 10"
msgstr "Пређи на лист терминала 10"

#: src/bin/keyin.c:694
msgid "Change title"
msgstr "Измени наслов"

#: src/bin/keyin.c:695 src/bin/keyin.c:696
msgid "Toggle whether input goes to all visible terminals"
msgstr ""

#: src/bin/keyin.c:699
msgid "Font size"
msgstr "Величина словног лика"

#: src/bin/keyin.c:700
msgid "Font size up 1"
msgstr "Повећај словни лик за 1"

#: src/bin/keyin.c:701
msgid "Font size down 1"
msgstr "Смањи величину словног лика за 1"

#: src/bin/keyin.c:702
msgid "Display big font size"
msgstr "Приказуј велики словни лик"

#: src/bin/keyin.c:703
msgid "Reset font size"
msgstr "_Врати величину словног лика"

#: src/bin/keyin.c:705
msgid "Actions"
msgstr "Радње"

#: src/bin/keyin.c:706
msgid "Open a new terminal window"
msgstr "Отвори нови прозор терминала"

#: src/bin/keyin.c:707
msgid "Toggle Fullscreen of the window"
msgstr "Прекидач за преко целог екрана"

#: src/bin/keyin.c:708
msgid "Display the history miniview"
msgstr "Прикажи мали преглед скорашњих"

#: src/bin/keyin.c:709
msgid "Display the command box"
msgstr "Прикажи наредбену кућицу"

#: src/bin/main.c:406 src/bin/main.c:602
#, fuzzy
msgid "Could not create window"
msgstr "Нисам успео да направим прозор"

#: src/bin/main.c:425 src/bin/main.c:613
#, fuzzy
msgid "Could not create terminal widget"
msgstr "Нисам успео отворити справицу терминала"

#: src/bin/main.c:460
#, c-format
msgid "(C) 2012-%d Carsten Haitzler and others"
msgstr "(C) 2012-%d Карстен Хедзла (Carsten Haitzler) и остали"

#: src/bin/main.c:462
#, fuzzy
msgid "Terminal emulator written with Enlightenment Foundation Libraries"
msgstr "Опонашач терминала написан по билиотекама задужбине Просвећења"

#: src/bin/main.c:466
#, fuzzy
msgid "Use the named file as a background wallpaper"
msgstr "Користи именовану датотеку као слику позадине"

#: src/bin/main.c:468
#, fuzzy
msgid "Change to directory for execution of terminal command"
msgstr "Промени фасциклу ради извршења наредбе терминала у њој"

#: src/bin/main.c:470
msgid "Command to execute. Defaults to $SHELL (or passwd shell or /bin/sh)"
msgstr ""
"Наредба за извршење. Подразумевано је $SHELL (или passwd shell или /bin/sh)"

#: src/bin/main.c:472
#, fuzzy
msgid "Use the named edje theme or path to theme file"
msgstr "Користи именовану едје тему или путању до датотеке теме"

#: src/bin/main.c:474
msgid "Use the named color scheme"
msgstr ""

#: src/bin/main.c:476
#, fuzzy
msgid "Terminal geometry to use (eg 80x24 or 80x24+50+20 etc.)"
msgstr "Размера терминала (нпр. 80x24 или 80x24+50+20 итд.)"

#: src/bin/main.c:478
#, fuzzy
msgid "Set window name"
msgstr "Постави име прозору"

#: src/bin/main.c:480
#, fuzzy
msgid "Set window role"
msgstr "Постави улогу прозора"

#: src/bin/main.c:482
#, fuzzy
msgid "Set window title"
msgstr "Постави наслов прозора"

#: src/bin/main.c:484
#, fuzzy
msgid "Set icon name"
msgstr "Постави име сличице"

#: src/bin/main.c:486
#, fuzzy
msgid "Set font (NAME/SIZE for scalable, NAME for bitmap)"
msgstr "Постави словни лик (ИМЕ/ВЕЛИЧИНА за променљив, ИМЕ за битмапу)"

#: src/bin/main.c:488
#, fuzzy
msgid ""
"Split the terminal window. 'v' for vertical and 'h' for horizontal. Can be "
"used multiple times. eg -S vhvv or --split hv More description available on "
"the man page"
msgstr ""
"Подели прозор терминала. „v“ је за водоравно, а „h“ за усправно. Може бити "
"коришћено више пута. НПР. -S vhvv или --split hv . Више описа је доступно на "
"man страници"

#: src/bin/main.c:493
#, fuzzy
msgid "Run the shell as a login shell"
msgstr "Покрећи љуску као љуску за пријаву"

#: src/bin/main.c:495
#, fuzzy
msgid "Set mute mode for video playback"
msgstr "Постави извођење видеа без звука"

#: src/bin/main.c:497
#, fuzzy
msgid "Set cursor blink mode"
msgstr "Постави да трепће показивач"

#: src/bin/main.c:499
#, fuzzy
msgid "Set visual bell mode"
msgstr "Укључи видно звоно"

#: src/bin/main.c:501
#, fuzzy
msgid "Go into the fullscreen mode from the start"
msgstr "Покрећи  у приказу преко целог екрана"

#: src/bin/main.c:503
msgid "Start iconified"
msgstr ""

#: src/bin/main.c:505
msgid "Start borderless"
msgstr ""

#: src/bin/main.c:507
#, fuzzy
msgid "Start as a override-redirect window"
msgstr "Начини прозор као преусмерени прозор"

#: src/bin/main.c:509
msgid "Start maximized"
msgstr ""

#: src/bin/main.c:511
#, fuzzy
msgid "Terminology is run without a window manager"
msgstr "Терминологија је покренута без управника прозора"

#: src/bin/main.c:513
#, fuzzy
msgid "Do not exit when the command process exits"
msgstr "Не излази када се изврши наредба"

#: src/bin/main.c:515
#, fuzzy
msgid "Force single executable if multi-instance is enabled"
msgstr "Приморај на једну извршну датотеку ако је омогућено више примерака"

#: src/bin/main.c:517
#, fuzzy
msgid "Set TERM to 'xterm-256color' instead of 'xterm'"
msgstr "Постави TERM на „xterm-256color“ уместо на „xterm“"

#: src/bin/main.c:519
msgid "Set scaling factor"
msgstr ""

#: src/bin/main.c:521
#, fuzzy
msgid "Highlight links"
msgstr "Истакни везе"

#: src/bin/main.c:523
msgid "Do not display wizard on start up"
msgstr ""

#: src/bin/main.c:549
#, fuzzy
msgid "show program version"
msgstr "прикажи издање програма"

#: src/bin/main.c:552
#, fuzzy
msgid "show copyright"
msgstr "прикажи права умножавања"

#: src/bin/main.c:555
#, fuzzy
msgid "show license"
msgstr "прикажи дозволу"

#: src/bin/main.c:558
#, fuzzy
msgid "show this message"
msgstr "прикажи ову поруку"

#: src/bin/main.c:655
#, fuzzy
msgid "invalid argument found for option -S/--split. See --help"
msgstr "неисправна примедба за могућност  -S/--split. Погледајте --help"

#: src/bin/main.c:909
#, fuzzy
msgid "Could not initialize key bindings"
msgstr "Нисам успео да покренем пречице"

#: src/bin/main.c:924
#, fuzzy
msgid "Could not parse command line options"
msgstr "Нисам успео да рашчланим могућности наредбене линије"

#: src/bin/main.c:948
#, c-format
msgid "option %s requires an argument!"
msgstr "могућност %s захтева примедбу!"

#: src/bin/main.c:949
#, fuzzy
msgid "invalid options found. See --help"
msgstr "неисправна могућност. Погледајте --help"

#: src/bin/media.c:1336 src/bin/termpty.c:649 src/bin/termpty.c:654
#: src/bin/termpty.c:658
#, c-format
msgid "Function %s failed: %s"
msgstr "Радња %s није успела: %s"

#: src/bin/media.c:1433
msgid "Media visualizing is not supported"
msgstr "Приказ садржаја није подржан"

#: src/bin/options_background.c:139
msgid "None"
msgstr "Ништа"

#: src/bin/options_background.c:471
msgid "Source file is target file"
msgstr "Изворна датотека је циљна датотека"

#: src/bin/options_background.c:484
msgid "Picture imported"
msgstr "Слика је увезена"

#: src/bin/options_background.c:489
msgid "Failed"
msgstr "Неуспех"

#: src/bin/options_background.c:541 src/bin/options.c:221
msgid "Background"
msgstr "Позадина"

#: src/bin/options_background.c:557
msgid "Translucent"
msgstr "Прозирност"

#: src/bin/options_background.c:568 src/bin/options_background.c:569
#, c-format
msgid "%1.0f%%"
msgstr "%1.0f%%"

#: src/bin/options_background.c:588
msgid "Select Path"
msgstr "Изабери путању"

#: src/bin/options_background.c:594
msgid "System"
msgstr "Систем"

#: src/bin/options_background.c:597
msgid "User"
msgstr "Корисник"

#: src/bin/options_background.c:600
msgid "Other"
msgstr "Друго"

#: src/bin/options_background.c:642
#, fuzzy
msgid "Click on a picture to use it as background"
msgstr "_Двоклик за увоз слике"

#: src/bin/options_behavior.c:63
#, c-format
msgid "%'d lines"
msgstr "%'d линија"

#: src/bin/options_behavior.c:79
#, c-format
msgid "Scrollback (current memory usage: %'.2f%cB):"
msgstr ""

#: src/bin/options_behavior.c:217
#, fuzzy
msgid "Default cursor:"
msgstr "Подразумевано"

#: src/bin/options_behavior.c:226
msgid "Blinking Block"
msgstr ""

#: src/bin/options_behavior.c:245
msgid "Steady Block"
msgstr ""

#: src/bin/options_behavior.c:265
msgid "Blinking Underline"
msgstr ""

#: src/bin/options_behavior.c:285
msgid "Steady Underline"
msgstr ""

#: src/bin/options_behavior.c:305
msgid "Blinking Bar"
msgstr ""

#: src/bin/options_behavior.c:325
msgid "Steady Bar"
msgstr ""

#: src/bin/options_behavior.c:365 src/bin/options.c:217
msgid "Behavior"
msgstr "Понашање"

#: src/bin/options_behavior.c:385
msgid "Show tabs"
msgstr "Прикажи листове"

#: src/bin/options_behavior.c:388
msgid "Scroll to bottom on new content"
msgstr "Клизни до дна на појаву новог садржаја"

#: src/bin/options_behavior.c:389
msgid "Scroll to bottom when a key is pressed"
msgstr "Клизни до дна када се притисне дугме"

#: src/bin/options_behavior.c:395
msgid "React to key presses (typing sounds and animations)"
msgstr ""

#: src/bin/options_behavior.c:402
msgid "Audio Support for key presses <failure>DISABLED</failure>!"
msgstr ""

#: src/bin/options_behavior.c:406
msgid "Visual Bell"
msgstr "Видно звоно"

#: src/bin/options_behavior.c:407
msgid "Bell rings"
msgstr "Звоно звони"

#: src/bin/options_behavior.c:408
msgid "Urgent Bell"
msgstr "Хитно звоно"

#: src/bin/options_behavior.c:410
msgid "Multiple instances, one process"
msgstr "Више примерака, један процес"

#: src/bin/options_behavior.c:411
msgid "Set TERM to xterm-256color"
msgstr "Постави ТЕРМ на икстерм-256боја"

#: src/bin/options_behavior.c:412
msgid "BackArrow sends Del (instead of BackSpace)"
msgstr "Стрелица брисања уназад шаље брисање (уместо брисања уназад)"

#: src/bin/options_behavior.c:413
msgid "Start as login shell"
msgstr "Покрени као љуску за пријаву"

#: src/bin/options_behavior.c:414
msgid "Open new terminals in current working directory"
msgstr "Отвори нови терминал у радној фасцикли"

#: src/bin/options_behavior.c:415
msgid "Always show miniview"
msgstr "Увек приказуј мали преглед"

#: src/bin/options_behavior.c:416
msgid "Enable special Terminology escape codes"
msgstr "Омогући нарочите излазне кодове Терминологији"

#: src/bin/options_behavior.c:417
msgid "Treat Emojis as double-width characters"
msgstr ""

#: src/bin/options_behavior.c:418
msgid ""
"When grouping input, do it on all terminals and not just the visible ones"
msgstr ""

#: src/bin/options_behavior.c:425
msgid "Always open at size:"
msgstr "Увек отвори у величини:"

#: src/bin/options_behavior.c:435
msgid "Set Current:"
msgstr "Постави тренутно:"

#: src/bin/options_behavior.c:446
msgid "Width:"
msgstr "Ширина:"

#: src/bin/options_behavior.c:468
msgid "Height:"
msgstr "Висина:"

#: src/bin/options_behavior.c:527
#, fuzzy
msgid ""
"Set the time of the animation that<br>takes places during tab switches,"
"<br>be those done due to key bindings or <br>mouse wheel over the tabs panel"
msgstr ""
"Постави време дејства оживљавања које<br>заузима место промене листова,"
"<br>било пречицом дугмади, точкићем<br>миша или променом мишем на површи "
"листова"

#: src/bin/options_behavior.c:532
msgid "Tab zoom/switch animation time:"
msgstr "Време дејства оживљавања приближавања/промене листа:"

#: src/bin/options_behavior.c:540 src/bin/options_behavior.c:541
#: src/bin/options_mouse.c:186 src/bin/options_mouse.c:187
#, c-format
msgid "%1.1f s"
msgstr "%1.1f s"

#: src/bin/options.c:189
msgid "Options"
msgstr "Могућности"

#: src/bin/options.c:218 src/bin/options_mouse.c:147
msgid "Mouse"
msgstr ""

#: src/bin/options.c:219 src/bin/options_font.c:499
msgid "Font"
msgstr "Словни лик"

#: src/bin/options.c:220 src/bin/options_theme.c:168
msgid "Theme"
msgstr "Тема"

#: src/bin/options.c:222
msgid "Colors"
msgstr "Боје"

#: src/bin/options.c:223
msgid "Keys"
msgstr "Дугмад"

#: src/bin/options.c:224 src/bin/options_elm.c:109
msgid "Toolkit"
msgstr "Алатница"

#: src/bin/options.c:235
msgid "Temporary"
msgstr "Привремено"

#: src/bin/options_colors.c:99
msgid "Open website"
msgstr ""

#: src/bin/options_colors.c:131
#, c-format
msgid "<b>Author: </b>%s<br/><b>Website: </b>%s<br/><b>License: </b>%s"
msgstr ""

#: src/bin/options_colors.c:226
#, fuzzy
msgid "Color schemes"
msgstr "Боје"

#: src/bin/options_colors.c:265
#, c-format
msgid "Using theme <hilight>%s</hilight>"
msgstr ""

#: src/bin/options_elm.c:122
#, c-format
msgid ""
"<em>Terminology</em> uses the <hilight>elementary</hilight> toolkit.<br>The "
"toolkit configuration settings can be accessed by running <keyword>%s</"
"keyword>"
msgstr ""

#: src/bin/options_elm.c:134
#, c-format
msgid "Launch %s"
msgstr ""

#: src/bin/options_elm.c:150 src/bin/win.c:384 src/bin/win.c:404
msgid "Scale"
msgstr ""

#: src/bin/options_elm.c:172 src/bin/win.c:433
msgid "Select preferred size so that this text is readable"
msgstr ""

#: src/bin/options_elm.c:180
msgid ""
"The scale configuration can also be changed through <hilight>elementary</"
"hilight>'s configuration panel"
msgstr ""

#: src/bin/options_font.c:550
msgid "Search font"
msgstr ""

#: src/bin/options_font.c:578
msgid "Bitmap"
msgstr "Битмапа"

#: src/bin/options_font.c:619
msgid "Standard"
msgstr "Уобичајено"

#: src/bin/options_font.c:681
msgid "Display bold and italic in the terminal"
msgstr "Приказуј задебљано и искошено у терминалу"

#: src/bin/options_keys.c:66
msgid "Ctrl+"
msgstr "Ктрл+"

#: src/bin/options_keys.c:67
msgid "Alt+"
msgstr "Мења+"

#: src/bin/options_keys.c:68
msgid "Shift+"
msgstr "Више+"

#: src/bin/options_keys.c:69
msgid "Win+"
msgstr "Супер+"

#: src/bin/options_keys.c:70
msgid "Meta+"
msgstr "Мета+"

#: src/bin/options_keys.c:71
msgid "Hyper+"
msgstr "Супер+"

#: src/bin/options_keys.c:82
msgid "Delete"
msgstr "Обриши"

#: src/bin/options_keys.c:270
msgid "Please press key sequence"
msgstr "Молим, притисните низ дугмади"

#: src/bin/options_keys.c:386
msgid "Key Bindings"
msgstr "Пречице дугмади"

#: src/bin/options_keys.c:458
msgid "Reset bindings"
msgstr "Врати пречице на подразумеване"

#: src/bin/options_mouse.c:167
msgid "Focus split under the Mouse"
msgstr "Дели под мишем"

#: src/bin/options_mouse.c:168
msgid "Focus-related visuals"
msgstr "Призори односни на жижу"

#: src/bin/options_mouse.c:174
msgid "Auto hide the mouse cursor when idle:"
msgstr ""

#: src/bin/options_mouse.c:204
#, fuzzy
msgid "Active Links:"
msgstr "Везе у погону"

#: src/bin/options_mouse.c:208
msgid "On emails"
msgstr ""

#: src/bin/options_mouse.c:209
msgid "On file paths"
msgstr ""

#: src/bin/options_mouse.c:210
#, fuzzy
msgid "On URLs"
msgstr "Отвори као адресу"

#: src/bin/options_mouse.c:211
#, fuzzy
msgid "On colors"
msgstr "Боје"

#: src/bin/options_mouse.c:212
#, fuzzy
msgid "Based on escape codes"
msgstr "Омогући нарочите излазне кодове Терминологији"

#: src/bin/options_mouse.c:213
msgid "Gravatar integration"
msgstr "Обједињавање граватара"

#: src/bin/options_mouse.c:215
msgid "Drag & drop links"
msgstr "Превуци и спусти везе"

#: src/bin/options_mouse.c:221
msgid "Inline if possible"
msgstr "У линији ако је могуће"

#: src/bin/options_mouse.c:265
msgid "E-mail:"
msgstr "Е.пошта:"

#: src/bin/options_mouse.c:269
msgid "URL (Images):"
msgstr "Адреса (слике):"

#: src/bin/options_mouse.c:270
msgid "URL (Video):"
msgstr "Адреса (снимка):"

#: src/bin/options_mouse.c:271
msgid "URL (All):"
msgstr "Адреса (свега):"

#: src/bin/options_mouse.c:274
msgid "Local (Images):"
msgstr "Месно (слике):"

#: src/bin/options_mouse.c:275
msgid "Local (Video):"
msgstr "Месно (снимака):"

#: src/bin/options_mouse.c:276
msgid "Local (All):"
msgstr "Месно (свега):"

#: src/bin/options_theme.c:187
#, c-format
msgid "Using colorscheme <hilight>%s</hilight>"
msgstr ""

#: src/bin/termcmd.c:84
#, c-format
msgid "Unknown font command: %s"
msgstr "Непозната наредба за словолик: %s"

#: src/bin/termcmd.c:118
#, c-format
msgid "Unknown grid size command: %s"
msgstr "Непозната наредба за величину мреже: %s"

#: src/bin/termcmd.c:146
#, c-format
msgid "Background file could not be read: %s"
msgstr "Нисам успео да прочитам позадинску датотеку: %s"

#: src/bin/termcmd.c:175
#, c-format
msgid "Unknown command: %s"
msgstr "Непозната наредба: %s"

#: src/bin/termio.c:459
#, c-format
msgid "Could not get working directory of pid %i: %s"
msgstr "Нисам успео да добавим радну фасциклу за пид %i: %s"

#: src/bin/termio.c:474
#, c-format
msgid "Could not load working directory %s: %s"
msgstr "Нисам успео да учитам радну фасциклу %s: %s"

#: src/bin/termio.c:1204
#, c-format
msgid "unsupported selection format '%s'"
msgstr "облик избора „%s“ није подржан"

#: src/bin/termio.c:1364
msgid "Preview"
msgstr "Преглед"

#: src/bin/termio.c:1374
msgid "Copy relative path"
msgstr "Умножи односну путању"

#: src/bin/termio.c:1376
msgid "Copy full path"
msgstr "Умножи општу путању"

#: src/bin/termio.c:2789
msgid "Open as URL"
msgstr "Отвори као адресу"

#: src/bin/termio.c:4232
msgid "Could not allocate termpty"
msgstr "Нисам успео да доделим меморију термптију"

#: src/bin/termpty.c:99 src/bin/termpty.c:141 src/bin/termpty.c:188
#, c-format
msgid "memerr: %s"
msgstr "гршкмем: %s"

#: src/bin/termpty.c:225
#, c-format
msgid "Size set ioctl failed: %s"
msgstr "Поставка величине иоцтл није успела: %s"

#: src/bin/termpty.c:361
#, c-format
msgid "Could not write to file descriptor %d: %s"
msgstr "Нисам успео да пишем у опису датотеке %d: %s"

#: src/bin/termpty.c:618
#, c-format
msgid "Could not find shell, falling back to %s"
msgstr "Нисам успео да пронађем љуску, замењујем је са %s"

#: src/bin/termpty.c:665
#, c-format
msgid "open() of pty '%s' failed: %s"
msgstr "отварање() птипсилон „%s“ није успело: %s"

#: src/bin/termpty.c:672 src/bin/termpty.c:678
#, c-format
msgid "fcntl() on pty '%s' failed: %s"
msgstr "фнцтл() на припсилон „%s“ није успело: %s"

#: src/bin/termpty.c:687
#, c-format
msgid "ioctl() on pty '%s' failed: %s"
msgstr "иоцтл() на пту „%s“ није успео: %s"

#: src/bin/termpty.c:728 src/bin/termpty.c:736 src/bin/termpty.c:745
#, c-format
msgid "Could not change current directory to '%s': %s"
msgstr "Нисам успео да пређем са тренутне фасцикле на „%s“: %s"

#: src/bin/theme.c:91
#, c-format
msgid "Could not load any theme for group=%s: %s"
msgstr "Нисам успео да учитам било коју тему скупа=%s: %s"

#: src/bin/win.c:425
msgid "Done"
msgstr ""

#: src/bin/win.c:441
msgid ""
"The scale configuration can be changed in the Settings (right click on the "
"terminal) →  Toolkit, or by starting the command <keyword>elementary_config</"
"keyword>"
msgstr ""

#: src/bin/win.c:2336
msgid "Ecore IMF failed"
msgstr "Неуспех ИМФ екора"

#: src/bin/win.c:6210
msgid "Ok"
msgstr "У реду"

#: src/bin/win.c:6215
msgid "Cancel"
msgstr "Откажи"

#: src/bin/win.c:7404
#, fuzzy
msgid "Couldn't find terminology theme! Forgot 'ninja install'?"
msgstr ""
"Нисам успео да пронађем тему Терминологије! Да нисте заборавили да урадите "
"„make install“?"

#~ msgid "React to key presses"
#~ msgstr "Одазови се на притисак дугмади"

#, fuzzy
#~ msgid "Become a borderless managed window"
#~ msgstr "Начини прозор без оквира"

#, fuzzy
#~ msgid "Become maximized from the start"
#~ msgstr "Покрећи као увећан прозор"

#, fuzzy
#~ msgid "Go into an iconic state from the start"
#~ msgstr "Покрећи у умањеном стању"

#~ msgid "Default"
#~ msgstr "Подразумевано"

#~ msgid "Black"
#~ msgstr "Црна"

#~ msgid "Red"
#~ msgstr "Црвена"

#~ msgid "Green"
#~ msgstr "Зелена"

#~ msgid "Yellow"
#~ msgstr "Жута"

#~ msgid "Blue"
#~ msgstr "Плава"

#~ msgid "Magenta"
#~ msgstr "Љубичаста"

#~ msgid "Cyan"
#~ msgstr "Плавичаста"

#~ msgid "White"
#~ msgstr "Бела"

#~ msgid "Inverse"
#~ msgstr "Изврнуто"

#, fuzzy
#~ msgid "Inverse Background"
#~ msgstr "Позадина"

#~ msgid "Normal"
#~ msgstr "Уобичајено"

#~ msgid "Bright/Bold"
#~ msgstr "Светло/Задебљано"

#~ msgid "Intense"
#~ msgstr "Јарко"

#~ msgid "Intense Bright/Bold"
#~ msgstr "Јарко Светло/Задебљано"

#~ msgid "Could not load default theme for group=%s: %s"
#~ msgstr "Нисам успео да учитам било коју тему скупа=%s: %s"

#~ msgid "Helpers"
#~ msgstr "Помоћници"

#~ msgid "Set emotion module to use."
#~ msgstr "Подеси која јединица емоушна ће да се користи."

#~ msgid "Could not Initialize the emotion module '%s'"
#~ msgstr "Нисам успео на покренем јединицу емоушна „%s“"

#~ msgid "Video"
#~ msgstr "Снимци"

#~ msgid "Audio muted"
#~ msgstr "Звук је утишан"

#~ msgid "Audio visualized"
#~ msgstr "Звук је оживљен сликом"

#~ msgid "Video Engine:"
#~ msgstr "Погон снимака:"

#~ msgid "Automatic"
#~ msgstr "Самостално"

#~ msgid "Cursor blinking"
#~ msgstr "Трепћући показивач"

#~ msgid "Scrollback:"
#~ msgstr "Клизање уназад:"

#~ msgid "Wallpaper"
#~ msgstr "Слика за позадину"

#~ msgid "Inverse Base"
#~ msgstr "Супротно од основе"

#~ msgid "Use"
#~ msgstr "Користи"

#~ msgid "Reset"
#~ msgstr "Врати на подразумевано"
